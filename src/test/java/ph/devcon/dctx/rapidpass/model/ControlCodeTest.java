package ph.devcon.dctx.rapidpass.model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests for ph.devcon.dctx.rapidpass.modelControlCode.
 */
public class ControlCodeTest {

    /**
     * Test ControlCode.encode() works.
     */
    @Test
    public void testControlCodeEncode() {
        final long[] n = {
                0,
                1,
                31,
                32,
                64,
                1024,
                1091,
                2491777155L
        };
        final String[] expected = {
                "00000000",
                "00000012",
                "000000ZV",
                "00000104",
                "00000208",
                "00001008",
                "00001236",
                "2A8B043S",
        };
        for (int i = 0; i < expected.length; ++i) {
            final String actual = ControlCode.encode(n[i]);
            assertEquals("ControlCode.encode(" + n[i] + ") returned " + actual + ", expected " + expected[i],
                    expected[i], actual);
        }
    }

    /**
     * Test for ControlCode.checkDigitIsValid()
     */
    @Test
    public void testCheckDigitIsValid() {
        assertTrue(ControlCode.checkDigitIsValid("2A8B043S"));
        assertFalse(ControlCode.checkDigitIsValid("2A8B043T"));
    }


    /**
     * Test ControlCode.decode() works.
     */
    @Test
    public void testControlCodeDecode() {
        final String[] controlCodes = {
                "00000000",
                "00000012",
                "000000ZV",
                "00000104",
                "00000208",
                "00001008",
                "00001236",
                "2A8B043S",
        };
        final long[] expected = {
                0,
                1,
                31,
                32,
                64,
                1024,
                1091,
                2491777155L
        };
        for (int i = 0; i < expected.length; ++i) {
            final String controlCode = controlCodes[i];
            final long actual = ControlCode.decode(controlCode);
            assertEquals("ControlCode.decode(\"" + controlCode + "\") returned " + actual + ", expected " + expected[i],
                    expected[i], actual);
        }
    }
}