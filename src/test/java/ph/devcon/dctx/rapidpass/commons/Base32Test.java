package ph.devcon.dctx.rapidpass.commons;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class Base32Test {

    /**
     * Test Base32.toRadixDigits(long) works.
     */
    @Test
    public void testToRadixDigits() {
        final long[] n = {
                0,
                1,
                31,
                32,
                64,
                1024,
                1091,
                2491777155L
        };
        final int[][] expected = {
                {0},
                {1},
                {31},
                {1, 0},
                {2, 0},
                {1, 0, 0},
                {1, 2, 3},
                {2, 10, 8, 11, 0, 4, 3}
        };
        for (int i = 0; i < expected.length; ++i) {
            final int[] actual = Base32.toRadixDigits(n[i]);
            assertArrayEquals("Damm32.toRadixDigits(" + n[i] + ") returned " + Arrays
                    .toString(actual) + ", expected " + Arrays.toString(expected[i]), expected[i], actual);
        }
    }
}