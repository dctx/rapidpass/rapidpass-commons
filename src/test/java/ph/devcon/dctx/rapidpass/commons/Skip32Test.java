package ph.devcon.dctx.rapidpass.commons;

import com.boivie.skip32.Skip32;
import org.bouncycastle.util.encoders.Base64;
import org.junit.Test;
import ph.devcon.dctx.rapidpass.model.ControlCode;

import javax.naming.ldap.Control;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class Skip32Test {

    /**
     * Test Skip32
     */
    @Test
    public void testSkip32() {
        final byte[] key = Base64.decode("fNDDGfFpEQKU4jEYnWxIB2LCdf4YVcBWdLl2NaqfPdg=");
        for (int i = 0; i < 100; ++i) {
            final int e = Skip32.encrypt(i, key);
        }
        assertEquals(99, Skip32.decrypt((int) ControlCode.decode("3AT940JA"), key));
    }
}