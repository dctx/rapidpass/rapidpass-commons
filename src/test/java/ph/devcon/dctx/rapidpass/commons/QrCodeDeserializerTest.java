package ph.devcon.dctx.rapidpass.commons;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Test;
import ph.devcon.dctx.rapidpass.model.QrCodeData;

import java.util.Base64;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit test for QrCodeEncoder.
 */
public class QrCodeDeserializerTest {

    private static final long CONTROL_CODE = 2491777155L;
    private static final int MAR_23_2020 = 1584921600;
    private static final int MAR_27_2020 = 1585267200;

    /**
     * Test QrCodeEncoder.decode() works with an added `name` field.
     */
    @Test
    public void decodeWithNameWorks() throws DecoderException {
        final byte[] bytes = Base64.getDecoder().decode("//5qFlQbr6l8SnZjKCxd5S9ejfETOduxiJHT7vqUIy9sHqdcAfh1u7X54A==");
        final QrCodeData expected = QrCodeData.individual().apor("GO").controlCode(CONTROL_CODE)
                .validFrom(MAR_23_2020)
                .validUntil(MAR_27_2020).idOrPlate("ABC1234").name("JUAN DELA CRUZ").build();
        final byte[] key = Hex.decodeHex("d099294ebdae6763ba75d386eae517aa");
        final QrCodeDeserializer deserializer = new QrCodeDeserializer(key);
        final QrCodeData actual = deserializer.decode(bytes);
        assertEquals(expected, actual);
    }

    /**
     * Test QrCodeEncoder.
     */
    @Test
    public void decodeWorks() throws DecoderException {
        final byte[] bytes = Base64.getDecoder().decode("//7qFlQbr6l8SnZjKCxd5S9ejfETOduxDCusjQ==");
        final QrCodeData expected = QrCodeData.vehicle().apor("GO").controlCode(CONTROL_CODE)
                .validFrom(MAR_23_2020)
                .validUntil(MAR_27_2020).idOrPlate("ABC1234").build();
        final byte[] key = Hex.decodeHex("d099294ebdae6763ba75d386eae517aa");
        final QrCodeDeserializer deserializer = new QrCodeDeserializer(key);
        final QrCodeData actual = deserializer.decode(bytes);
        assertEquals(expected, actual);
    }

    @Test
    public void testSerdeWithNameWorks() throws DecoderException {
        final byte[] key = Hex.decodeHex("d099294ebdae6763ba75d386eae517aa");
        final byte[] hmacKeyData = Hex.decodeHex("a993cb123b3ba87bf06c22365bfa0951e2521fcff5990b42dacf7c4b55e83a42");
        final Signer signer = HmacSha256.signer(hmacKeyData);

        final QrCodeData data = QrCodeData.vehicle().apor("GO").controlCode(CONTROL_CODE).validFrom(MAR_27_2020)
                .validUntil(MAR_27_2020).idOrPlate("ABC1234").name("JUAN DELA CRUZ").build();
        assertNotNull(data);
        final byte[] bytes = new QrCodeSerializer(key).serializeAndSign(data, signer);
        assertNotNull(bytes);
        assertEquals(bytes.length, 43);
        assertEquals("fffeea16541bafa97c40c863282c5de52f5e8df11339dbb18891d3eefa94232f6c1ea75c01f875a7013d4d",
                Hex.encodeHexString(bytes));
        final QrCodeData actual = new QrCodeDeserializer(key).decode(bytes);
        assertEquals(data, actual);
    }

    @Test
    public void testSerdeWorks() throws DecoderException {
        final byte[] key = Hex.decodeHex("d099294ebdae6763ba75d386eae517aa");
        final byte[] hmacKeyData = Hex.decodeHex("a993cb123b3ba87bf06c22365bfa0951e2521fcff5990b42dacf7c4b55e83a42");
        final Signer signer = HmacSha256.signer(hmacKeyData);

        final QrCodeData data = QrCodeData.vehicle().apor("GO").controlCode(CONTROL_CODE).validFrom(MAR_27_2020)
                .validUntil(MAR_27_2020).idOrPlate("ABC1234").build();
        assertNotNull(data);
        final byte[] bytes = new QrCodeSerializer(key).serializeAndSign(data, signer);
        assertNotNull(bytes);
        assertEquals(bytes.length, 28);
        assertEquals("fffeea16541bafa97c40c863282c5de52f5e8df11339dbb19f85bdd8", Hex.encodeHexString(bytes));
        final QrCodeData actual = new QrCodeDeserializer(key).decode(bytes);
        assertEquals(data, actual);
    }
}
