package ph.devcon.dctx.rapidpass.commons;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class RsaTest {

    /**
     * Test RsaSigner.sign() works
     */
    @Test
    public void testSign() throws DecoderException, IOException {
        final byte[] bytes = Hex.decodeHex("c74f948580835e77fc005e7d42000741424331323334");
        final byte[] digest = Rsa.signer("./etc/keys/private").sign(bytes);
        System.out.println(Hex.encodeHex(digest));
        final byte[] expected = Hex
                .decodeHex("239d9785850e85d43ca892db9bec116fd142245e1fcde406a087540febce4ec6" +
                        "ef429b9116fcd78ee9a35ed85909b16a66551b3fd1066ae1b112797a0a62f2d6" +
                        "969fd2a43b64e3d6f061e8a16977fe8cee4515d82f5830a9669bef90f176e901" +
                        "32b53692b81794fd3367a790809090bd7e24ead86d991af92dd4936a1ddc4750");
        assertArrayEquals(expected, digest);
    }
}