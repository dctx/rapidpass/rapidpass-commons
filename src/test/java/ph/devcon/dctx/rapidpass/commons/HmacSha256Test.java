package ph.devcon.dctx.rapidpass.commons;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class HmacSha256Test {

    /**
     * Test HmacSha256.sign() works
     */
    @Test
    public void testSign() throws DecoderException, IOException {
        final byte[] keyData = Hex.decodeHex("a993cb123b3ba87bf06c22365bfa0951e2521fcff5990b42dacf7c4b55e83a42");
        final byte[] data = Hex.decodeHex("c74f948580835e77fc005e7d42000741424331323334");
        final byte[] digest = HmacSha256.signer(keyData).sign(data);
        System.out.println(Hex.encodeHex(digest));
        final byte[] expected = Hex.decodeHex("6dcd000f2100cb5c2b026f0eab623deb81dbe6e964b413ae7f192f7e8204f23b");
        assertArrayEquals(expected, digest);
    }
}