package ph.devcon.dctx.rapidpass.commons;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;

public class Damm32Test {

    /**
     * Test Damm32.compute(int n) works.
     */
    @Test
    public void testCompute() {
        final long[] n = {
                0,
                1,
                31,
                32,
                64,
                1024,
                1091,
                2491777155L
        };
        final int[] expected = {
                0,
                2,
                27,
                4,
                8,
                8,
                6,
                25
        };
        for (int i = 0; i < expected.length; ++i) {
            final int actual = Damm32.compute(n[i]);
            assertEquals("Damm32.compute(" + n[i] + ") returned " + actual + ", expected " + expected[i], expected[i], actual);
        }
    }

    /**
     * Test Damm32.computeFromDigits(int[] digits) works.
     */
    @Test
    public void testComputeFromDigits() {
        final int[][] digits = {
                {},
                {0},
                {1},
                {31},
                {1, 0},
                {2, 0},
                {1, 0, 0},
                {1, 2, 3},
                {2, 10, 8, 11, 0, 4, 3}
        };
        final int[] expected = {
                0,
                0,
                2,
                27,
                4,
                8,
                8,
                6,
                25
        };
        for (int i = 0; i < expected.length; ++i) {
            final int actual = Damm32.computeFromDigits(digits[i]);
            assertEquals(Arrays.toString(digits[i]) + " => " + actual, expected[i], actual);
        }
    }

}