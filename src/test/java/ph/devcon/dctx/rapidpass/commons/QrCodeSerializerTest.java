package ph.devcon.dctx.rapidpass.commons;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import ph.devcon.dctx.rapidpass.model.QrCodeData;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import static org.junit.Assert.*;

/**
 * Unit test for QrCodeEncoder.
 */
public class QrCodeSerializerTest {

    private static final long CC_1234_ENCRYPTED = 2491777155L;
    private static final int APRIL_26_2020 = DateTimeUtils.dateTimeInSeconds(2020, 4, 26, 0, 0);
    private static final int MAY_15_2020 = DateTimeUtils.dateTimeInSeconds(2020, 5, 15, 0, 0);

    /**
     * Test QrCodeEncoder#serializeAndSign() with an added name.
     */
    @Test
    public void serializeAndSignWithNameWorks() throws DecoderException {
        System.out.println((byte) 200);
        final QrCodeData data = QrCodeData.individual().apor("GO").controlCode(CC_1234_ENCRYPTED).validFrom(
                APRIL_26_2020)
                .validUntil(MAY_15_2020).idOrPlate("ABC1234").name("JUAN DELA CRUZ").build();
        assertNotNull(data);
        // TODO: In production, key data should come from the environment (either hex or Base64, or read from a file
        final byte[] hmacKeyData = Hex.decodeHex("a993cb123b3ba87bf06c22365bfa0951e2521fcff5990b42dacf7c4b55e83a42");
        final Signer signer = HmacSha256.signer(hmacKeyData);
        final byte[] key = Hex.decodeHex("d099294ebdae6763ba75d386eae517aa");
        final byte[] bytes = new QrCodeSerializer(key).serializeAndSign(data, signer);
        assertNotNull(bytes);
        assertEquals(bytes.length, 43);
        assertEquals("fffe6a16541bafa97c99d4e328ec74e52f5e8df11339dbb18891d3eefa94232f6c1ea75c01f8751ffbfa73",
                Hex.encodeHexString(bytes));
        assertEquals("//5qFlQbr6l8mdTjKOx05S9ejfETOduxiJHT7vqUIy9sHqdcAfh1H/v6cw==",
                Base64.getEncoder().encodeToString(bytes));
    }

    /**
     * Test QrCodeEncoder#serializeAndSign() works.
     */
    @Test
    public void serializeAndSignWorks() throws DecoderException {
        final QrCodeData data = QrCodeData.vehicle().apor("GO").controlCode(CC_1234_ENCRYPTED).validFrom(APRIL_26_2020)
                .validUntil(MAY_15_2020).idOrPlate("ABC1234").build();
        assertNotNull(data);
        // TODO: In production, key data should come from the environment (either hex or Base64, or read from a file
        final byte[] hmacKeyData = Hex.decodeHex("a993cb123b3ba87bf06c22365bfa0951e2521fcff5990b42dacf7c4b55e83a42");
        final Signer signer = HmacSha256.signer(hmacKeyData);
        final byte[] key = Hex.decodeHex("d099294ebdae6763ba75d386eae517aa");
        final byte[] bytes = new QrCodeSerializer(key).serializeAndSign(data, signer);
        assertNotNull(bytes);
        assertEquals(bytes.length, 28);
        assertEquals("fffeea16541bafa97c99d4e328ec74e52f5e8df11339dbb104efd3fb", Hex.encodeHexString(bytes));
        assertEquals("//7qFlQbr6l8mdTjKOx05S9ejfETOduxBO/T+w==",
                Base64.getEncoder().encodeToString(bytes));
    }

    /**
     * Test QrCodeEncoder.
     */
    @Test
    public void serializeWorks() {
        final QrCodeData data = QrCodeData.vehicle().apor("GO").controlCode(CC_1234_ENCRYPTED).validFrom(APRIL_26_2020)
                .validUntil(MAY_15_2020).idOrPlate("ABC1234").build();
        assertNotNull(data);
        final byte[] bytes = QrCodeSerializer.serialize(data);
        assertNotNull(bytes);
        assertEquals(bytes.length, 22);
        assertEquals("c74f948580835ea45e805ebd6b000741424331323334", Hex.encodeHexString(bytes));
    }


    @Test
    public void testAesCtr() throws DecoderException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        final byte[] key = Hex.decodeHex("d099294ebdae6763ba75d386eae517aa");
        final byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        final SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        final IvParameterSpec ivSpec = new IvParameterSpec(iv);

        final Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivSpec);
        final byte[] plainText = Hex.decodeHex("c74f948580835e77fc005e7d42000741424331323334");
        final byte[] cipherText = cipher.doFinal(plainText);
        System.out.println("cipherText.length => " + cipherText.length);
        final byte[] forBase64 = new byte[cipherText.length + 2];
        forBase64[0] = (byte) 0xff;
        forBase64[1] = (byte) 0xfe;
        System.arraycopy(cipherText, 0, forBase64, 2, cipherText.length);
        System.out.println("forBase64: " + Hex.encodeHexString(forBase64) + " (" + forBase64.length + ")");
        System.out.println(Base64.getEncoder().encodeToString(forBase64));

        final Cipher decrypt = Cipher.getInstance("AES/CTR/NoPadding");
        decrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, ivSpec);
        final byte[] decrypted = decrypt.doFinal(cipherText);
        System.out.println("decrypted: " + Hex.encodeHexString(decrypted));
        assertArrayEquals(plainText, decrypted);
    }

    @Test
    public void testAes() throws DecoderException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        final String secret = "abc";
        final byte[] key = Hex.decodeHex("d099294ebdae6763ba75d386eae517aa");

        final SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

        final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        final byte[] plainText = Hex.decodeHex("c74f948580835e77fc005e7d42000741424331323334");
        final byte[] cipherText = cipher.doFinal(plainText);
        System.out.println("cipherText.length => " + cipherText.length);
        final byte[] forBase64 = new byte[cipherText.length + 2];
        forBase64[0] = (byte) 0xff;
        forBase64[1] = (byte) 0xfe;
        System.arraycopy(cipherText, 0, forBase64, 2, cipherText.length);
        System.out.println("forBase64: " + Hex.encodeHexString(forBase64) + " (" + forBase64.length + ")");
        System.out.println(Base64.getEncoder().encodeToString(forBase64));

        final Cipher decrypt = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        decrypt.init(Cipher.DECRYPT_MODE, secretKeySpec);
        final byte[] decrypted = decrypt.doFinal(cipherText);
        System.out.println("decrypted: " + Hex.encodeHexString(decrypted));
        assertArrayEquals(plainText, decrypted);
    }

}
