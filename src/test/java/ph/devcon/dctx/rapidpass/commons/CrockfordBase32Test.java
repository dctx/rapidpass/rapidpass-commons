package ph.devcon.dctx.rapidpass.commons;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class CrockfordBase32Test {

    /**
     * Test CrockfordBase32.encode(long n) works.
     */
    @Test
    public void testEncode() {
        final long[] n = {
                0,
                1,
                31,
                32,
                1091,
                987654321,
                2491777155L
        };
        final String[] expected = {
                "0000000",
                "0000001",
                "000000Z",
                "0000010",
                "0000123",
                "0XDWT5H",
                "2A8B043"
        };
        for (int i = 0; i < expected.length; ++i) {
            final String actual = CrockfordBase32.encode(n[i], 7);
            assertEquals("CrockfordBase32.encode(" + n[i] + ") returned " + actual + ", expected " + expected[i],
                    expected[i], actual);
        }
    }
}
