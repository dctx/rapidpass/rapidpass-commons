package ph.devcon.dctx.rapidpass.model;

import java.util.Objects;

/**
 * An immutable POJO/DTO (data transfer object) to hold the QR Code data.
 *
 * @author Alistair A. Israel
 */
public final class QrCodeData {
    private final boolean vehiclePass;
    private final String aporCode;
    private final long controlCode;
    private final int validFrom;
    private final int validUntil;
    private final String idOrPlate;
    private final String name;

    /**
     * Constructs an immutable QrCodeData.
     *
     * @param vehiclePass `true` if this is a vehicle pass
     * @param aporCode    `'AA'` to `'ZZ'` (todo: checking)
     * @param controlCode 32-bit integer
     * @param validFrom   32-bit seconds from epoch
     * @param validUntil  32-bit seconds from epoch
     * @param idOrPlate   String
     * @param name        String
     */
    QrCodeData(final boolean vehiclePass, final String aporCode, final long controlCode, final int validFrom,
               final int validUntil, final String idOrPlate, final String name) {
        // TODO bounds checking
        this.vehiclePass = vehiclePass;
        this.aporCode = aporCode.toUpperCase();
        this.controlCode = controlCode;
        this.validFrom = validFrom;
        this.validUntil = validUntil;
        this.idOrPlate = idOrPlate.toUpperCase();
        this.name = name != null ? name.toUpperCase() : name;
    }

    /**
     * Creates a Builder for a vehicle pass.
     *
     * @return Builder
     */
    public static Builder vehicle() {
        final Builder builder = new Builder();
        builder.vehiclePass = true;
        return builder;
    }

    /**
     * Creates a Builder for an individual pass.
     *
     * @return Builder
     */
    public static Builder individual() {
        final Builder builder = new Builder();
        builder.vehiclePass = false;
        return builder;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("QrCodeData{vehiclePass=");
        sb.append(this.vehiclePass);
        sb.append(", aporCode=");
        sb.append(this.aporCode);
        sb.append(", controlCode=");
        sb.append(this.controlCode);
        sb.append(", validFrom=");
        sb.append(this.validFrom);
        sb.append(", validUntil=");
        sb.append(this.validUntil);
        sb.append(", idOrPlate=\"");
        sb.append(this.idOrPlate);
        sb.append("\", name=");
        if (this.name == null) {
            sb.append("null");
        } else {
            sb.append('\"');
            sb.append(this.name);
            sb.append('\"');
        }
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final QrCodeData that = (QrCodeData) o;
        return this.vehiclePass == that.vehiclePass && Objects.equals(this.aporCode, that.aporCode)
                && this.controlCode == that.controlCode && this.validFrom == that.validFrom
                && this.validUntil == that.validUntil && Objects.equals(this.idOrPlate, that.idOrPlate) && Objects
                .equals(this.name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.vehiclePass, this.aporCode, this.controlCode, this.validFrom, this.validUntil,
                this.idOrPlate, this.name);
    }

    /**
     * @return the vehiclePass
     */
    public boolean isVehiclePass() {
        return this.vehiclePass;
    }

    /**
     * @return the aporCode
     */
    public String getAporCode() {
        return this.aporCode;
    }

    /**
     * @return the controlCode
     */
    public long getControlCode() {
        return this.controlCode;
    }

    /**
     * @return the validFrom
     */
    public int getValidFrom() {
        return this.validFrom;
    }

    /**
     * @return the validUntil
     */
    public int getValidUntil() {
        return this.validUntil;
    }

    /**
     * @return the idOrPlate
     */
    public String getIdOrPlate() {
        return this.idOrPlate;
    }

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Builder pattern.
     */
    public static final class Builder {
        private Boolean vehiclePass;
        private String aporCode;
        private Long controlCode;
        private Integer validFrom;
        private Integer validUntil;
        private String idOrPlate;
        private String name;


        /**
         * Set the vehicle flag.
         *
         * @param vehiclePass boolean
         * @return this
         */
        public Builder vehiclePass(final boolean vehiclePass) {
            this.vehiclePass = vehiclePass;
            return this;
        }

        /**
         * Set the aporCode.
         *
         * @param aporCode String
         * @return this
         */
        public Builder apor(final String aporCode) {
            // TODO bounds checking
            this.aporCode = aporCode;
            return this;
        }

        /**
         * Set the controlCode.
         *
         * @param controlCode long
         * @return this
         */
        public Builder controlCode(final long controlCode) {
            this.controlCode = controlCode;
            return this;
        }

        /**
         * Set the validFrom.
         *
         * @param validFrom int
         * @return this
         */
        public Builder validFrom(final int validFrom) {
            this.validFrom = validFrom;
            return this;
        }

        /**
         * Set the validUntil.
         *
         * @param validUntil int
         * @return this
         */
        public Builder validUntil(final int validUntil) {
            this.validUntil = validUntil;
            return this;
        }

        /**
         * Set the idOrPlate.
         *
         * @param idOrPlate String
         * @return this
         */
        public Builder idOrPlate(final String idOrPlate) {
            this.idOrPlate = idOrPlate;
            return this;
        }

        /**
         * Set the name.
         *
         * @param name String
         * @return this
         */
        public Builder name(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Assemble the QrCodeData.
         *
         * @return QrCodeData
         */
        public QrCodeData build() {
            return new QrCodeData(this.vehiclePass, this.aporCode, this.controlCode, this.validFrom, this.validUntil,
                    this.idOrPlate, this.name);
        }
    }
}
