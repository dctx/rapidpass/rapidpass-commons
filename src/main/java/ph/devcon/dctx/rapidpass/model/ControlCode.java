package ph.devcon.dctx.rapidpass.model;

import ph.devcon.dctx.rapidpass.commons.CrockfordBase32;
import ph.devcon.dctx.rapidpass.commons.Damm32;

/**
 * Because `concat(encode(), encode(checkdigit()))` is hard.
 */
public final class ControlCode {

    private static final long UINT32_MASK = 0xffffffffL;
    private final long value;

    /**
     * Construct a ControlCode with the given long value.
     *
     * @param value the value
     */
    public ControlCode(final long value) {
        this.value = value;
    }

    /**
     * Encode a control code number into Crockford Base32 with Damm32 check digit.
     *
     * @param longControlCodeNumber the ControlCode `long` value
     * @return the ControlCode for display as a String
     */
    public static String encode(final long longControlCodeNumber) {
        final String base32Crockford = CrockfordBase32.encode(longControlCodeNumber, 7);
        final int damm32 = Damm32.compute(longControlCodeNumber);
        final String checkDigit = CrockfordBase32.encode(damm32);
        return base32Crockford + checkDigit;
    }

    /**
     * Encode a control code number into Crockford Base32 with Damm32 check digit.
     *
     * @param controlCodeNumber the ControlCode `long` value
     * @return the ControlCode for display as a String
     */
    public static String encode(final int controlCodeNumber) {
        return encode(((long) controlCodeNumber) & UINT32_MASK);
    }

    /**
     * Check if the given ControlCode String has a valid check digit.
     *
     * @param controlCode the ControlCode String
     * @return `true` if the given ControlCode String has a valid check digit
     */
    public static boolean checkDigitIsValid(final String controlCode) {
        final int length = controlCode.length();
        final String base = controlCode.substring(0, length - 1);
        final String actual = controlCode.substring(length - 1, length);
        final long controlCodeNumber = CrockfordBase32.decode(base);
        final int damm32 = Damm32.compute(controlCodeNumber);
        final String checkDigit = CrockfordBase32.encode(damm32);
        return checkDigit.equals(actual);
    }


    /**
     * Decode a String ControlCode to a long.
     *
     * @param controlCode the ControlCode String
     * @return the ControlCode as a `long`
     * @throws IllegalArgumentException if the given ControlCode has an invalid check digit, or invalid Base32 digits
     */
    public static long decode(final String controlCode) {
        final int length = controlCode.length();
        final String base = controlCode.substring(0, length - 1);
        final String actual = controlCode.substring(length - 1, length);
        final long controlCodeNumber = CrockfordBase32.decode(base);
        final int damm32 = Damm32.compute(controlCodeNumber);
        final String checkDigit = CrockfordBase32.encode(damm32);
        if (!checkDigit.equals(actual)) {
            throw new IllegalArgumentException("Control code \"" + controlCode + "\" has invalid check digit!");
        }
        return controlCodeNumber;
    }

    /**
     * Parse a String into a ControlCode.
     *
     * @param controlCode the ControlCode String
     * @return the ControlCode
     * @throws IllegalArgumentException if the given ControlCode has an invalid check digit, or invalid Base32 digits
     */
    public static ControlCode parse(final String controlCode) {
        return new ControlCode(decode(controlCode));
    }

    /**
     * Return this ControlCode's integer value.
     *
     * @return the value
     */
    public long getValue() {
        return this.value;
    }

    /**
     * Compute and return the Damm32 check digit of this ControlCode's value.
     *
     * @return the check digit
     */
    public int getCheckDigit() {
        return Damm32.compute(this.value);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ControlCode that = (ControlCode) o;
        return this.value == that.value;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(this.value);
    }

    @Override
    public String toString() {
        // CHECKSTYLE:OFF MagicNumber
        return CrockfordBase32.encode(this.value, 7) + CrockfordBase32.encode(getCheckDigit());
        // CHECKSTYLE:ON MagicNumber
    }
}
