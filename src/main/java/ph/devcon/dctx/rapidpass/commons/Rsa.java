package ph.devcon.dctx.rapidpass.commons;

import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

/**
 * Just has one utility method to convert a long to int[] Base32 digits.
 */
public final class Rsa {

    static {
        java.security.Security.addProvider(
                new org.bouncycastle.jce.provider.BouncyCastleProvider()
        );
    }

    /**
     * Utility classes should not have a public or default constructor.
     */
    private Rsa() {
        // noop
    }

    /**
     * Return an Rsa.Signer with the private key loaded from the given filename.
     *
     * @param privateKeyFilename the filename holding the private key PEM
     * @return Signer
     */
    public static Signer signer(final String privateKeyFilename) throws IOException {
        final FileReader reader = new FileReader(privateKeyFilename);
        final PemReader pemReader = new PemReader(reader);
        try {
            final PemObject pemObject = pemReader.readPemObject();
            final byte[] pemContent = pemObject.getContent();
            return new RsaSigner(pemContent);
        } finally {
            pemReader.close();
        }
    }

    /**
     * The RSA Signer.
     */
    public static final class RsaSigner implements Signer {
        private final PrivateKey privateKey;

        /**
         * Construct an Rsa.Signer using the given bytes as private key data.
         *
         * @param keyData the private key data
         */
        public RsaSigner(final byte[] keyData) {
            final PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(keyData);
            try {
                final KeyFactory kf = KeyFactory.getInstance("RSA");
                this.privateKey = kf.generatePrivate(encodedKeySpec);
            } catch (final NoSuchAlgorithmException | InvalidKeySpecException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public byte[] sign(final byte[] bytes) {
            try {
                final Signature privateSignature = Signature.getInstance("SHA256withRSA");
                privateSignature.initSign(this.privateKey);
                privateSignature.update(bytes);
                return privateSignature.sign();
            } catch (final NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

