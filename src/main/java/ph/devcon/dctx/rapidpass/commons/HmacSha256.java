package ph.devcon.dctx.rapidpass.commons;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Hex;

import java.security.Provider;
import java.security.Security;

/**
 * Just has one utility method to convert a long to int[] Base32 digits.
 */
public final class HmacSha256 {

    static {
        Provider found = null;
        for (final Provider provider : Security.getProviders()) {
            if (provider instanceof org.bouncycastle.jce.provider.BouncyCastleProvider) {
                found = provider;
                break;
            }
        }
        if (found == null) {
            Security.addProvider(
                    new org.bouncycastle.jce.provider.BouncyCastleProvider()
            );
        }
    }

    /**
     * Utility classes should not have a public or default constructor.
     */
    private HmacSha256() {
        // noop
    }

    /**
     * Return an HmacSha256.Signer with the private key bytes.
     *
     * @param keyData the HMAC signing key bytes
     * @return Signer
     */
    public static Signer signer(final byte[] keyData) {
        return new HmacSha256Signer(keyData);
    }

    /**
     * The HMAC SHA-256 Signer.
     */
    public static final class HmacSha256Signer implements Signer {

        private static final byte[] PADDING = Hex
                .decode("c60531e24aa5c747788c435ae9851b5410e769752cc6a9ebb60e5b12196d32ad");

        private final byte[] keyData;

        /**
         * Construct an Rsa.Signer using the given bytes as private key data.
         *
         * @param keyData the private key data
         */
        public HmacSha256Signer(final byte[] keyData) {
            this.keyData = keyData;
        }

        @Override
        public byte[] sign(final byte[] bytes) {
            final HMac hmac = new HMac(new SHA256Digest());
            hmac.init(new KeyParameter(this.keyData));
            final byte[] result = new byte[hmac.getMacSize()];
            hmac.update(bytes, 0, bytes.length);
            hmac.doFinal(result, 0);
            return result;
        }
    }
}

