package ph.devcon.dctx.rapidpass.commons;

public final class CrockfordBase32 {

    private static final String SYMBOLS = "0123456789ABCDEFGHJKMNPQRSTVWXYZ";

    /**
     * Utility classes should not have a public or default constructor.
     */
    private CrockfordBase32() {
        // noop
    }

    /**
     * Encode a number using Crockford Base32 encoding, then pad left with '0' to the specified length.
     *
     * @param n the number to encode
     * @return String
     */
    public static String encode(final long n) {
        return encode(n, 0);
    }

    /**
     * Encode a number using Crockford Base32 encoding, then pad left with '0' to the specified length.
     *
     * @param n           the number to encode
     * @param padToLength should be 8
     * @return String
     */
    public static String encode(final long n, final int padToLength) {
        final int[] digits = Base32.toRadixDigits(n);
        final int length = digits.length;
        final StringBuilder buf = new StringBuilder(padToLength);
        for (int i = padToLength - length; i > 0; --i) {
            buf.append('0');
        }
        for (int i = 0; i < length; ++i) {
            buf.append(SYMBOLS.charAt(digits[i]));
        }
        return buf.toString();
    }

    /**
     * Decode a Crockford Base32 String into a long.
     *
     * @param crockfordBase32 Crockford Base32 String
     * @return long
     */
    public static long decode(final String crockfordBase32) {
        long l = 0;
        for (final int digit : toDigits(crockfordBase32)) {
            // CHECKSTYLE:OFF MagicNumber
            l = (l << 5) + digit;
            // CHECKSTYLE:ON MagicNumber
        }
        return l;
    }

    /**
     * Decode a Crockford Base32 String into an int[] of Base32 digits.
     *
     * @param crockfordBase32 the Crockford Base32 encoded String
     * @return int[] digits
     */
    public static int[] toDigits(final String crockfordBase32) {
        final int length = crockfordBase32.length();
        final int[] digits = new int[length];
        for (int i = 0; i < length; ++i) {
            final char ch = crockfordBase32.charAt(i);
            final int index = SYMBOLS.indexOf(ch);
            if (index < 0) {
                throw new IllegalArgumentException("Illegal Crockford Base32 digit '" + ch + "' at index " + i);
            }
            digits[i] = index;
        }
        return digits;
    }
}
