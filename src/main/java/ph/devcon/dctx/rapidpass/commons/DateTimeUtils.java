package ph.devcon.dctx.rapidpass.commons;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * A utility class that provides a utility method for easily getting a local date+time in seconds since epoch.
 */
public final class DateTimeUtils {

    /**
     * The "Asia/Manila" time zone.
     */
    public static final ZoneId ASIA_MANILA = ZoneId.of("Asia/Manila");

    /**
     * Utility classes should not have a public or default constructor.
     */
    private DateTimeUtils() {
        // noop
    }

    /**
     * Return the DateTime in milliseconds for the given date, relative to the ASIA_MANILA time zone.
     *
     * @param year  the year
     * @param month the month (1..12)
     * @param day   the day (1..31)
     * @param hh    the hour (0..23)
     * @param mm    the minute (0..59)
     * @return int
     */
    public static int dateTimeInSeconds(final int year, final int month, final int day, final int hh, final int mm) {
        final LocalDateTime ldt = LocalDateTime.of(year, month, day, hh, mm, 0);
        final ZonedDateTime zdt = ZonedDateTime.of(ldt, ASIA_MANILA);
        return (int) zdt.toEpochSecond();
    }
}
