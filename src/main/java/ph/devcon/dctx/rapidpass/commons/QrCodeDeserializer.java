package ph.devcon.dctx.rapidpass.commons;

import ph.devcon.dctx.rapidpass.model.QrCodeData;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

/**
 * The QrCodeSerializer serializes the given QrCodeData into a byte[] array.
 *
 * @author Alistair A. Israel
 */
public final class QrCodeDeserializer {

    /**
     * The minimum byte[] length for the encryptionKeyBytes and initialisationVectorBytes.
     */
    public static final int MINIMUM_KEY_LENGTH = 16;


    // Use a hard-coded '0' IV
    // Please don't actually do this for anything requiring real cryptographic security
    // We're just doing this to 'mask' the QR Code data
    private static final byte[] ZERO_IV_BYTES = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    private static final long INT_MASK = 0xffffffffL;
    private static final int MSB_MASK = 0x80;
    private static final int REST_OF_BYTE_MASK = 0x7f;

    private final byte[] encryptionKeyBytes;

    /**
     * Construct a QrCodeDeserializer with the given key bytes and IV bytes.
     *
     * @param encryptionKeyBytes byte[], ideally from the environment (use Hex.decodeHex or Base64)
     */
    public QrCodeDeserializer(final byte[] encryptionKeyBytes) {
        if (encryptionKeyBytes.length < MINIMUM_KEY_LENGTH) {
            throw new IllegalArgumentException("Encryption key needs to be " + MINIMUM_KEY_LENGTH + " bytes or longer");
        }
        this.encryptionKeyBytes = encryptionKeyBytes;
    }

    private static String getStringFromBytes(final ByteBuffer buf, final int offset) {
        final int len = buf.get(offset);
        final byte[] idOrPlateBytes = new byte[len];
        buf.position(offset + 1);
        buf.get(idOrPlateBytes, 0, len);
        return new String(idOrPlateBytes);
    }

    /**
     * Decrypt the encrypted ciper text using the configured secret key and initialisation vector.
     *
     * @param cipherText byte[]
     * @return the plainText byte[]
     */
    public byte[] decrypt(final byte[] cipherText) {
        final SecretKeySpec secretKeySpec = new SecretKeySpec(this.encryptionKeyBytes, "AES");
        final IvParameterSpec ivspec = new IvParameterSpec(ZERO_IV_BYTES);

        try {
            final Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivspec);
            return cipher.doFinal(cipherText);
        } catch (final GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Serialise the QrCodeData into a byte[] array.
     *
     * @param bytes the QrCodeData
     * @return QrCodeData
     */
    public QrCodeData decode(final byte[] bytes) {
        // CHECKSTYLE:OFF MagicNumber
        if (bytes[0] != ((byte) 0xff) && bytes[1] != ((byte) 0xfe)) {
            throw new IllegalArgumentException("Invalid QR Code data!");
        }
        final byte[] cipherText = new byte[bytes.length - 2];
        System.arraycopy(bytes, 2, cipherText, 0, bytes.length - 2);
        final byte[] plainText = decrypt(cipherText);
        final ByteBuffer buf = ByteBuffer.wrap(plainText);
        final byte firstByte = buf.get(0);
        final boolean vehiclePass = (MSB_MASK == (firstByte & MSB_MASK));
        final char aporCode0 = (char) (firstByte & REST_OF_BYTE_MASK);
        final char aporCode1 = (char) buf.get(1);
        final String aporCode = String.valueOf(aporCode0) + String.valueOf(aporCode1);
        final long controlCode = buf.getInt(2) & INT_MASK;
        final int validFrom = buf.getInt(6);
        final int validUntil = buf.getInt(10);
        final String idOrPlate = getStringFromBytes(buf, 14);
        final QrCodeData.Builder builder = new QrCodeData.Builder().vehiclePass(vehiclePass).apor(aporCode)
                .controlCode(controlCode)
                .validFrom(validFrom).validUntil(validUntil).idOrPlate(idOrPlate);
        if (plainText.length > 15 + idOrPlate.length() + 4) {
            builder.name(getStringFromBytes(buf, 15 + idOrPlate.length()));
        }
        // CHECKSTYLE:ON MagicNumber
        return builder.build();
    }

}
