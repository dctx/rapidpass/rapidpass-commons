package ph.devcon.dctx.rapidpass.commons;

import ph.devcon.dctx.rapidpass.model.QrCodeData;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * The QrCodeSerializer serializes the given QrCodeData into a byte[] array.
 *
 * @author Alistair A. Israel
 */
public final class QrCodeSerializer {

    /**
     * The minimum byte[] length for the encryptionKeyBytes and initialisationVectorBytes.
     */
    public static final int MINIMUM_KEY_LENGTH = 16;

    /**
     * The maximum number of characters to be encoded for names, idOrPlate, etc.
     */
    public static final int MAX_STRING_LENGTH = 80;

    private static final int MOST_SIGNIFICANT_BIT_ON = 0x80;
    private static final int BYTE_MASK = 0xFF;
    // Use a hard-coded '0' IV
    // Please don't actually do this for anything requiring real cryptographic security
    // We're just doing this to 'mask' the QR Code data
    private static final byte[] ZERO_IV_BYTES = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    private final byte[] encryptionKeyBytes;

    /**
     * Construct a QrCodeSerializer with the given key bytes and IV bytes.
     *
     * @param encryptionKeyBytes byte[], ideally from the environment (use Hex.decodeHex or Base64)
     */
    public QrCodeSerializer(final byte[] encryptionKeyBytes) {
        if (encryptionKeyBytes.length < MINIMUM_KEY_LENGTH) {
            throw new IllegalArgumentException("Encryption key needs to be " + MINIMUM_KEY_LENGTH + " bytes or longer");
        }
        this.encryptionKeyBytes = encryptionKeyBytes;
    }

    /**
     * Truncates the given stringh to MAX_STRING_LENGTH.
     *
     * @param s a String
     * @return `null` if `s` is `null`, `s` if the length of `s` is less than MAX_STRING_LENGTH,
     * otherwise, `s` truncated to MAX_STRING_LENGTH
     */
    private static String truncateToMaxLength(final String s) {
        if (s == null) {
            return null;
        }
        if (s.length() < MAX_STRING_LENGTH) {
            return s;
        } else {
            return s.substring(0, MAX_STRING_LENGTH);
        }
    }

    /**
     * Serialise the QrCodeData into a byte[] array.
     *
     * @param data the QrCodeData
     * @return byte[]
     */
    public static byte[] serialize(final QrCodeData data) {
        final byte[] idOrPlateBytes = truncateToMaxLength(data.getIdOrPlate()).getBytes(UTF_8);
        final byte[] nameBytes = data.getName() != null ? truncateToMaxLength(data.getName()).getBytes(UTF_8) : null;
        final int bufferSize = 14 + idOrPlateBytes.length + 1 + (nameBytes != null ? nameBytes.length + 1 : 0);
        final ByteBuffer buf = ByteBuffer.allocate(bufferSize);
        final byte msb = (byte) (data.isVehiclePass() ? MOST_SIGNIFICANT_BIT_ON : 0x00);
        final String aporCode = data.getAporCode();
        final byte aporCode0 = (byte) (aporCode.charAt(0) & BYTE_MASK);
        final byte typeAndPurpose = (byte) (msb | aporCode0);
        buf.put(typeAndPurpose);
        buf.put((byte) aporCode.charAt(1));
        buf.putInt((int) data.getControlCode());
        buf.putInt(data.getValidFrom());
        buf.putInt(data.getValidUntil());
        buf.put((byte) idOrPlateBytes.length);
        buf.put(idOrPlateBytes);
        if (nameBytes != null) {
            buf.put((byte) nameBytes.length);
            buf.put(nameBytes);
        }
        return buf.array();
    }

    /**
     * Serialize and encrypt the data.
     *
     * @param data the raw, serialized QrCodeData
     * @return encrypted byte[]
     */
    public byte[] serializeAndEncrypt(final QrCodeData data) {
        final byte[] plainText = QrCodeSerializer.serialize(data);
        final SecretKeySpec secretKeySpec = new SecretKeySpec(this.encryptionKeyBytes, "AES");
        final IvParameterSpec ivspec = new IvParameterSpec(ZERO_IV_BYTES);

        try {
            final Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivspec);
            final byte[] cipherText = cipher.doFinal(plainText);
            final byte[] forBase64 = new byte[cipherText.length + 2];
            // CHECKSTYLE:OFF MagicNumber
            // Magic marker bytes
            forBase64[0] = (byte) 0xff;
            forBase64[1] = (byte) 0xfe;
            // CHECKSTYLE:ON MagicNumber
            System.arraycopy(cipherText, 0, forBase64, 2, cipherText.length);
            return forBase64;
        } catch (final GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Serialize then sign the QR Code data.
     *
     * @param data   QrCodeData
     * @param signer Signer
     * @return the serialized and signed bytes
     */
    public byte[] serializeAndSign(final QrCodeData data, final Signer signer) {
        final byte[] serialized = serializeAndEncrypt(data);
        final byte[] signature = signer.sign(serialized);
        final int length = serialized.length;
        // CHECKSTYLE:OFF MagicNumber
        final ByteBuffer buf = ByteBuffer.allocate(length + 4);
        // CHECKSTYLE:ON MagicNumber
        buf.put(serialized);
        buf.position(length);
        // CHECKSTYLE:OFF MagicNumber
        buf.put(signature, 0, 4);
        // CHECKSTYLE:ON MagicNumber

        return buf.array();
    }

}
