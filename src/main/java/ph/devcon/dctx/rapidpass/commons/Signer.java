package ph.devcon.dctx.rapidpass.commons;

/**
 * A common Signer interface to allow swapping out of signature algorithms / implmentations later on.
 */
public interface Signer {

    /**
     * Sign something.
     *
     * @param data the byte[] to sign
     * @return the byte[] signature
     */
    byte[] sign(byte[] data);
}
