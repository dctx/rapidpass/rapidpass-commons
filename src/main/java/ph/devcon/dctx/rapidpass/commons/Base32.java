package ph.devcon.dctx.rapidpass.commons;

import java.util.ArrayList;
import java.util.List;

/**
 * Just has one utility method to convert a long to int[] Base32 digits.
 */
public final class Base32 {

    private static final int SHIFT_5 = 5;
    private static final int MASK = 0x1F;

    /**
     * Utility classes should not have a public or default constructor.
     */
    private Base32() {
        // noop
    }

    /**
     * Convert an integer to a int[] of Base32 digits.
     *
     * @param n int
     * @return int[] of Base32 digits
     */
    public static int[] toRadixDigits(final long n) {
        final List<Integer> list = toRadixDigitsList(n);
        // TODO: A better way to convert List<Integer> to int[]
        final int size = list.size();
        final int[] array = new int[size];
        for (int i = 0; i < size; ++i) {
            array[i] = list.get(i);
        }
        return array;
    }

    // TODO: Refactor for efficiency.
    private static List<Integer> toRadixDigitsList(final long n) {
        final long rem = n >> SHIFT_5; // div 32
        final int digit = (int) (n & MASK);
        final List<Integer> list;
        if (rem > 0) {
            list = toRadixDigitsList(rem);
        } else {
            list = new ArrayList<Integer>(1);
        }
        list.add(digit);
        return list;
    }
}
