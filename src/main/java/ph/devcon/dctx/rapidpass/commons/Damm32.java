package ph.devcon.dctx.rapidpass.commons;

public final class Damm32 {

    // CHECKSTYLE:OFF MagicNumber
    private static final int MODULUS = 1 << 5; // 32
    private static final int MASK = MODULUS | 5; // 32 | mask[5 - 2] => 32 | 5 => 37
    // CHECKSTYLE:ON MagicNumber

    /**
     * Utility classes should not have a public or default constructor.
     */
    private Damm32() {
        // noop
    }

    /**
     * <p>Compute and return the Base32 Damm check digit for an int.</p>
     * <p>
     * For reference, see:
     * https://stackoverflow.com/questions/23431621/extending-the-damm-algorithm-to-base-32#answer-23433934
     * </p>
     *
     * @param n int
     * @return the Base32 Damm check digit
     */
    public static int compute(final long n) {
        return computeFromDigits(Base32.toRadixDigits(n));
    }

    /**
     * Compute and return the Base32 Damm check digit from an array of Base32 digits.
     * <p>
     * For reference, see:
     * https://stackoverflow.com/questions/23431621/extending-the-damm-algorithm-to-base-32#answer-23433934
     *
     * @param digits int[]
     * @return the Base32 Damm check digit
     */
    public static int computeFromDigits(final int[] digits) {
        int checkDigit = 0;
        for (final int digit : digits) {
            checkDigit = nextDigit(checkDigit, digit);
        }
        return checkDigit;
    }

    private static int nextDigit(final int checkDigit, final int digit) {
        int next = checkDigit;
        next ^= digit;
        next <<= 1;
        if (next >= MODULUS) {
            next ^= MASK;
        }
        return next;
    }

}
