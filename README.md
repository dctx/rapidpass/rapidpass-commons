RapidPass Commons (Java)
====

A set of common / utility classes in Java for Rapidpass.ph.

# Examples

## QrCodeSerializer

```java
QrCodeData data = QrCodeData.vehicle()
    .purpose('V')
    .controlCode(CC_1234_ENCRYPTED)
    .validFrom(MAR_23_2020)
    .validUntil(MAR_27_2020)
    .idOrPlate("ABC1234").build();
byte[] bytes = QrCodeSerializer.encode(data);
```

Will yield `bytes` as:
```
d6 94 85 80 83 5e 77 fc 00 5e 7d 42 00 0e 41 42 43 31 32 33 34
```